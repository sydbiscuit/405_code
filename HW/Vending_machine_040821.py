# Author: Sydney Lewis
# Date: 04/08/21

# Main script for FSM

## Keyboard loop for user input

#import ME405_HW01_LEWIS_040621.py



import keyboard 

last_key = ''
    

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
 
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("q", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)
keyboard.on_release_key("enter", callback=kb_cb)

# Run this loop forever, or at least until someone presses 'q'
# Code similar to this will go into specific states of your vendotron FSM

priceDict = {'c':100, 'p':120, 's':85, 'd':110, '0':0}

from time import sleep
 
def VendotronTask():
    """ Runs one iteration of the task
    """
    global last_key 
    state = 0
    bev_sel = '0'
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        if last_key == '0':
                last_key = ''
                print("Penny entered.")
                state = 1
        elif last_key == '1':
                last_key = ''
                print("Nickle entered.")
                state = 1
        elif last_key == '2':
                last_key = ''
                print("Dime entered.")
                state = 1
        elif last_key == '3':
                last_key = ''
                print("Quarter entered.")
                state = 1
        elif last_key == '4':
                last_key = ''
                print("One dollar entered.")
                state = 1
        elif last_key == '5':
                last_key = ''
                print("Five dollars entered.")
                state = 1
        elif last_key == '6':
                last_key = ''
                print("Ten dollars entered.")
                state = 1
        elif last_key == '7':
                last_key = ''
                print("Twenty dollars entered.")
                state = 1
        elif last_key == 'c':
                last_key = ''
                bev_sel = 'c'
                print("Cuke selected.")
                state = 2
        elif last_key == 'p':
                last_key = ''
                bev_sel = 'p'
                print("Popsy selected.")
                state = 2
        elif last_key == 's':
                last_key = ''
                bev_sel = 's'
                print("Spryte selected.")
                state = 2
        elif last_key == 'd':
                last_key = ''
                bev_sel = 'd'
                print("Dr. Pupper selected.")
                state = 2
        elif last_key == 'enter':
                last_key = ''
                bev_sel = '0'
                print("Payment entered.")
                state = 1
        elif last_key == 'e':
                last_key = ''
                eject = 'e'
                print("Ejecting change...")
                state = 3
        elif last_key == 'q':
                last_key = ''
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                state = 0
                break
        
        
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print("Select beverage at any time by pressing 'c', 'p', 's' or 'd'.")
            print("Return change by pressing 'e'.")
            

            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            
            
            
                
                
                
            state = 2      # on the next iteration, the FSM will run state 1
        
                
        elif state == 2:
            if bev_sel == '0':
                state = 1
            elif bev_sel != '0':
                cost = priceDict[bev_sel]
                print('Cost is {:d} cents. Please enter payment by pressing keys 1-7. Press enter when finished.'.format (cost))
                bev_sel = '0' 
                state = 1      # on the next iteration, the FSM will run state 1
                
            
                
             
                
            
        elif state == 3:
            # perform state 3 operations
            #print("The state is",state)
            state = 0   # s3 -> s1
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)


if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try: 
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')