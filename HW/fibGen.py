def fibSequence(numItems):
    
    # Find seed #0 first
    f0 = 0
    yield(f0)
    
    # Find seed #1
    f1 = 1
    yield(f1)
    
    # Find the general Fibonacci term
    counter = 2
    while(counter < numItems):
        counter += 1  # equivalent to counter = counter + 1
        # comment out the counter to get it to indefinitely run through the
        # fib sequence
        # can also do while(True):
    
        f2 = f1 + f0  # calculating new fib number
        
        f0 = f1  # keep track of most recent two fib numbers for next iteration
        f1 = f2
        
        yield(f2)
    
if __name__== "__main__":
    MySequence = fibSequence(10)
    
    # print(next(MySequence))
    # print(next(MySequence))
    # print(next(MySequence))
    # print(next(MySequence))
    
    for fibNum in MySequence:
        print(fibNum)
        
        
mySequence = fibSequence(100)
myList = list(mySequence)

for fibNum in myList:
    print('The number is {:d}'.format(fibNum))
    
