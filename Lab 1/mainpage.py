'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 

   @mainpage

   @section sec_intro   Introduction
                        The first assignment in ME 405 is designed to get
                        students familarized with finite-state-machines (FSM).

   @section sec_lab1    Lab0x01
                        For Lab0x01, a soda beverage vending machine was
                        programmed in Python. The program allows users to 
                        select beverages, enter their payment in $0.01 - $20
                        denominations, and compute the balance while allowing 
                        coin ejection at any time. The initial FSM diagram can be seen
                        here
                        \image html FSM_Lab1.png width=800cm height=600cm 
                        although the final program has some changes from this design that 
                        are not reflected in the diagram.                
                        
                        Note: Please see https://bitbucket.org/sydbiscuit/405_code/src/master/Lab%201/Lab0x01.py
                        for the code shown in detail. See https://bitbucket.org/sydbiscuit/405_code/src/master/Lab%201/mainpage.py
                        for the mainpage.

   @section sec_foobar  foobar
                        This is a program that generates "bar" when you "foo." 
                        Note: Please see https://bitbucket.org/sydbiscuit/405_code/src/master/Lab%201/foobar.py 
                        for details.

   @author              Sydney Lewis

   @copyright           sydbiscuit 

   @date                04/20/21
'''