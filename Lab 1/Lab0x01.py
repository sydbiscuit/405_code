"""@file            Lab0x01.py
   @brief           This file contains a FSM example using a vending machine.
   @author          Sydney Lewis
   @date            04/14/21
   
   @package         Lab0x01
   @brief           This package contains the entire lab script for Lab 1.
   @author          Sydney Lewis
   @author          Charlie Refvem
   @date            April 14, 2021
"""


## The first line of code sets up a dictionary to hold the vending machine
## beverages and their corresponding price in cents.
drinkDict = {'c':('Cuke',100), 'p':('Popsi',120), 's':('Spryte',85),
             'd':('Dr. Pupper',110), '0':('Nothing',0)}

## A dictionary to hold payment entries and corresponding value
## in cents.
coinDict = {'0':('Penny',1), '1':('Nickle',5), '2':('Dime',10), '3':('Quarter',25), 
            '4':('One dollar',100), '5':('Five dollars',500), '6':('Ten dollars',1000), 
            '7':('Twenty dollars',2000)}

## A dictionary to separate the eject button from the 
## coin and drink buttons.
Eject = {'e':('Eject',0)}

## List of denomination names and values
denoms = [['Penny',       1],
          ['Nickle',      5],
          ['Dime',       10],
          ['Quarter',    25],
          ['$1 Bill',   100],
          ['$5 Bill',   500],
          ['$10 Bill', 1000],
          ['$20 Bill', 2000]]

## Keyboard loop for user input
import keyboard 

## This command resets previous keyboard input and prepares to accept next key input
last_key = ''
    
def kb_cb(key):
    """ @brief              Callback function which is called when a key has been pressed.
        @details            When a user inputs any of the designated keys, this function will
                            assign the variable "last_key" the prescribed letter.
        @param key          locating and associating a letter with the keyboard.
    """
    global last_key
 
    last_key = key.name

## Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("q", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

def getChange(price, pmt):
    '''
    @brief          Computes change for a monetary transaction.
    @details        Given the price of an item and the denominations to pay
                    with, computes the proper change using the fewest number of
                    denominations.
    @param price    The price of the item to purchase as an integer number of
                    cents.
    @param pmt      A list containing the quantity for each denomination.
                    Denominations are ordered smallest to largest: pennies, 
                    nickels, dimes, quarters, ones, fives, tens, and twenties.
    @return         If the payment is sufficient, returns a  list representing
                    the computed change in the same format as pmt. If payment
                    is insufficient, returns None.
    @author         Charlie Refvem
    
    '''
    

# Compute total value of provided denominations
    pmtVal = sum(denomQty*denom[1] for denomQty, denom in zip(pmt,denoms))
    
    # Return early if there are insufficient funds
    if pmtVal < price:
        return None
    
    # If funds are sufficient, compute required change
    cngVal = pmtVal - price
    
    # Start an empty list to fill with denomination quantities
    cng = []
    
    # Starting with the biggest denomination, compute quantity of each.
    for denom in reversed(denoms):
        cng.insert(0, cngVal // denom[1])
        cngVal -= denom[1]*cng[0]
        
    return cng

# Run this loop forever, or at least until someone presses 'q'
# Code similar to this will go into specific states of your vendotron FSM

from time import sleep
 
def VendotronTask():
    """ @brief      Continuously runs the vending machine functions.
        @details    This code loops indefinitely through the vending machine states, waiting for a key pressed
                    to be pressed. The user can press keys to select a beverage, input a payment, 
                    and request a refund. The program has a built in advertisement substate (1a), that
                    prompts the user to "Try Cuke Today!" when idle for more than 3.1s. This can be 
                    modified by changing the "counter" variable in state 1. When inputing payment, the balance
                    is automatically updated and displayed in the console. If the user is idle for more than 2.5s
                    with a remaining balance, the program will prompt the user to select a second beverage, or 
                    request the machine for change. This timer can be modified in state 2 - Updating and Printing Balance.
                    Note: This code utilizes the user's direct payment input to modify the change received if more money has  
                    been added and ejected prior to the user selecting another beverage. This means that the "getChange" function
                    is not called, and in some instances, the change ejected may not always be optimized by the smallest denominations
                    possible, but rather it will eject what the user supplied. This is a desirable feature a real vending machine would 
                    utilize, avoiding errors of attempting to dispense denominations that the vending machine did not have. Instead
                    it would simply dispense what the user gave it in the first place, ensuring no physical payment errors occur.
        @author     Sydney Lewis

    """
    # Clear variables used inside all states
    global last_key 
    state = 0
    ## Holds the current selected beverage key.
    bev_sel = '0'
    ## Counter for advertisement. Increases by 1 each time it travels through state 1 without a button press.
    counter = 0
    ## Holds the current balance. 
    balance = 0
    ## Counter for second beverage selection when balance remains but the user is idle. Increases by 1 each time it travels 
    ## through state 2 with a non-zero balance.
    counter2 = 0
    ## Stores the change calculated by the getChange function.
    change = 0
    ## Holds the denominations the user inputs for payment.
    pmt = [0,0,0,0,0,0,0,0]
    
    # FSM main loop
    while True:
        
        ## State 0: Initialization Message
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print("")
            print("Welcome to Vendotron! Press 'c' for Cuke ($1.00), 'p' for Popsi ($1.20),")
            print("'s' for Spryte ($0.85), or 'd' for Dr. Pupper ($1.10)!")
            print("")
            print("Enter payment by pressing keys 0-7. Return change by pressing 'e'.") 
            print("[Pennies, Nickles, Dimes, Quarters, Dollars, Fives, Tens, Twenties]")
            print("")
            

            state = 1       # on the next iteration, the FSM will run state 1
          
        ## State 1: Checking for button presses   
        if state == 1:
            if counter == 310:
                print("Try Cuke Today!")
                counter = 0
            elif last_key in coinDict.keys(): # if the key pressed is inside the payment dictionary,
                pay_sel = last_key            # make a new variable to hold the key
                last_key = ''                 # clear last_key to wait for new button press
                counter = 0                   # reset the advertisement counter when payment is entered
                state = 2                     # send the next state to updating and printing balance
            elif last_key in drinkDict.keys():
                bev_sel = last_key
                last_key = ''
                counter = 0
                state = 3
            elif last_key == 'e':
                eject = last_key
                last_key = ''
                state = 4
                counter = 0
            elif last_key == 'q':
                last_key = ''
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                state = 0
            elif balance != 0:
                counter += 1
                state = 2
            else:
                counter += 1
                state = 1
                
        
        ## State 2: Updating and printing balance (updating payment list); counting for 2nd prompt  
        elif state == 2:
            if counter2 == 250:
                print('Balance of\t${:.2f} remaining. Please select another beverage. Or "e" for change'.format(balance/100))
                counter2 = 0
                state = 1
            elif pay_sel == 'z':
                counter2 += 1
                state = 1
            else:
                state = 1      # on the next iteration, the FSM will go back to state 1
                counter2 = 0
                balance += coinDict[pay_sel][1]
                print('You have entered:', coinDict[pay_sel][0])
                print('Current Balance:\t${:.2f}'.format(balance/100))
                # Indexing pmt list to add inserted denominations for getChange function
                if pay_sel == '0':
                    pmt[0] = int(pmt[0]) + 1
                    pay_sel = 'z'
                elif pay_sel == '1':
                    pmt[1] = int(pmt[1]) + 1
                    pay_sel = 'z'
                elif pay_sel == '2':
                    pmt[2] = int(pmt[2]) + 1
                    pay_sel = 'z'
                elif pay_sel == '3':
                    pmt[3] = int(pmt[3]) + 1
                    pay_sel = 'z'
                elif pay_sel == '4':
                    pmt[4] = int(pmt[4]) + 1
                    pay_sel = 'z'
                elif pay_sel == '5':
                    pmt[5] = int(pmt[5]) + 1
                    pay_sel = 'z'
                elif pay_sel == '6':
                    pmt[6] = int(pmt[6]) + 1
                    pay_sel = 'z'
                elif pay_sel == '7':
                    pmt[7] = int(pmt[7]) + 1
                    pay_sel = 'z'
                elif eject == 'e':
                    state = 3
                    
                
        
        ## State 3: Beverage selection and change calculation based on payment list  
        elif state == 3:
            price = drinkDict[bev_sel][1]
            print('You have selected:', drinkDict[bev_sel][0])
            print('Price:\t${:.2f}'.format(price/100))
            state = 1
            if balance == 0: # if balance is zero and beverage is selected, give insufficient funds
                print('Insufficient Funds')
                print('Current Balance:\t${:.2f}'.format(balance/100))
            else:
                bev_sel = '0' 
                counter2 = 0
                change = getChange(price, pmt)  # if balance != 0, run getChange function
                if change == None:
                    print("Insufficient Funds - Add money or select a different beverage.")
                    print('Current Balance:\t${:.2f}'.format(balance/100))
                    change = 0
                    state = 1
                else:
                    balance -= price   # update balance
                    print('New balance:\t${:.2f}'.format(balance/100))
                    pmt = change       # update pmt list
                    if pay_sel == 'e':
                        state = 4
                        pay_sel = 'z'
                    else:
                        state = 1
                
         # perform state 4 operations: Eject change   
        elif state == 4:
            if balance == 0:
                print("No change to eject.")
                state = 0
            elif change != 0:
                print('Ejecting change:\t${:.2f}'.format(balance/100))
                print(change)
                balance = 0
                pmt = [0,0,0,0,0,0,0,0]
                state = 0   # s4 -> s0
            else:
                print('Ejecting change:\t${:.2f}'.format(balance/100))
                print(pmt)
                pmt = [0,0,0,0,0,0,0,0]
                balance = 0
                state = 0   # s4 -> s0
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)


if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try: 
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')

