# This file implements an example student class

class student:
    ''' @brief Brief description of the class
    '''
    
    def __init__(self, f_name, l_name):
        ''' @brief Brief description of constructor
        '''
        
        self.first_name = f_name    # assigning input paramater f_name
                                    # to a class attribute, so we can keep
                                    # the data persistenly and access it in
                                    # any methods
                                    
        self.last_name = l_name
       
    def get_email_address(self):
        #email = self.first_name + "." + self.last_name + "@calpoly.edu"
        email = '{first}.{last}@calpoly.edu'.format(fist=self.first_name, last=self.last_name)
        
        return email
    